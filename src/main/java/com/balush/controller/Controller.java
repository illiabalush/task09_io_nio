package com.balush.controller;

import com.balush.model.Tasks;
import com.balush.view.View;

public class Controller {
    private Tasks tasks;

    public Controller(View view) {
        tasks = new Tasks(view);
    }

    public Tasks getTasks() {
        return tasks;
    }
}
