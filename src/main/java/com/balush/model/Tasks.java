package com.balush.model;

import com.balush.model.serializable.ExtendClass;
import com.balush.model.clientserver.client.Client;
import com.balush.view.View;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ResourceBundle;

public class Tasks {

    private View view;
    private ResourceBundle resourceBundle;

    public Tasks(View view) {
        this.view = view;
        resourceBundle = ResourceBundle.getBundle("path");
    }

    public String task01() {
        String path = resourceBundle.getString("task01");
        ExtendClass extendClass = new ExtendClass(10, "Second", 3.14, 2.32, "Last");
        writeClass(path, extendClass);
        ExtendClass extendClass2 = readClass(path);
        return extendClass2.toString();
    }

    private void writeClass(String path, ExtendClass object) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(path))) {
            objectOutputStream.writeObject(object);
        } catch (IOException ex) {
            view.showException(ex);
        }
    }

    private ExtendClass readClass(String path) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream(path))) {
            return (ExtendClass) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            view.showException(ex);
        }
        return null;
    }

    public String task02() {
        String path = resourceBundle.getString("task02");
        StringBuilder stringBuilder = new StringBuilder();
        String[] result = getTimeReadWithoutBuffer(path);
        stringBuilder.append("Time without buffer: ").append(Integer.valueOf(result[0]) / 1000).append(" s")
                .append(" | ").append(Integer.valueOf(result[1]) / 1000000).append(" MB").append("\n");

        result = getTimeReadWithBuffer(path, 1000000);
        stringBuilder.append("Time with buffer 1MB: ").append(Integer.valueOf(result[0])).append(" ms")
                .append(" | ").append(Integer.valueOf(result[1]) / 1000000).append(" MB").append("\n");

        result = getTimeReadWithBuffer(path, 5000000);
        stringBuilder.append("Time with buffer 5MB: ").append(Integer.valueOf(result[0])).append(" ms")
                .append(" | ").append(Integer.valueOf(result[1]) / 1000000).append(" MB").append("\n");

        result = getTimeReadWithBuffer(path, 10000000);
        stringBuilder.append("Time with buffer 10MB: ").append(Integer.valueOf(result[0])).append(" ms")
                .append(" | ").append(Integer.valueOf(result[1]) / 1000000).append(" MB").append("\n");

        result = getTimeReadWithBuffer(path, 15000000);
        stringBuilder.append("Time with buffer 15MB: ").append(Integer.valueOf(result[0])).append(" ms")
                .append(" | ").append(Integer.valueOf(result[1]) / 1000000).append(" MB").append("\n");

        result = getTimeReadWithBuffer(path, 8192);
        stringBuilder.append("Time with standart buffer: ").append(Integer.valueOf(result[0])).append(" ms")
                .append(" | ").append(Integer.valueOf(result[1]) / 1000000).append(" MB").append("\n");
        return stringBuilder.toString();
    }

    private String[] getTimeReadWithoutBuffer(String path) {
        long startTime = System.currentTimeMillis();
        long countBytes = 0;
        try (InputStream inputStream = new FileInputStream(path)) {
            while (inputStream.read() != -1) {
                countBytes++;
            }
        } catch (IOException ex) {
            view.showException(ex);
        }
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        return new String[]{String.valueOf(elapsedTime), String.valueOf(countBytes)};
    }

    private String[] getTimeReadWithBuffer(String path, int sizeBuffer) {
        long startTime = System.currentTimeMillis();
        long countBytes = 0;
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(
                new FileInputStream(path), sizeBuffer)) {
            while (bufferedInputStream.read() != -1) {
                countBytes++;
            }
        } catch (IOException ex) {
            view.showException(ex);
        }
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        return new String[]{String.valueOf(elapsedTime), String.valueOf(countBytes)};
    }

    public String task03() {
        String path = resourceBundle.getString("task03");
        String allCode = getCodeFromFile(path);
        StringBuilder allComments = new StringBuilder();
        String comment = "";
        for (int i = 0; i < allCode.length() - 1; i++) {
            if (allCode.charAt(i) == '/' && allCode.charAt(i + 1) == '*') {
                comment = parseExtendedComment(allCode, i);
                allComments.append(comment).append("\n\n");
            } else if (allCode.charAt(i) == '/' && allCode.charAt(i + 1) == '/') {
                comment = parseSimpleComment(allCode, i);
                allComments.append(comment).append("\n\n");
            }
            i += comment.length();
            comment = "";
        }
        return allComments.toString();
    }

    private String getCodeFromFile(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            int c;
            while ((c = bufferedReader.read()) != -1) {
                stringBuilder.append((char) c);
            }
        } catch (IOException ex) {
            view.showException(ex);
        }
        return stringBuilder.toString();
    }

    private String parseExtendedComment(String code, int position) {
        StringBuilder stringBuilder = new StringBuilder();
        while (code.charAt(position) != '*' || code.charAt(position + 1) != '/') {
            stringBuilder.append(code.charAt(position));
            position++;
        }
        stringBuilder.append(code.charAt(position));
        stringBuilder.append(code.charAt(position + 1));
        return stringBuilder.toString();
    }

    private String parseSimpleComment(String code, int position) {
        StringBuilder stringBuilder = new StringBuilder();
        position--;
        do {
            position++;
            stringBuilder.append(code.charAt(position));
        } while (code.charAt(position) != '\n' && position < code.length() - 1);
        return stringBuilder.toString();
    }

    public String task04() {
        String path = resourceBundle.getString("task04");
        File file = new File(path);
        return showDirectory(file, 0);
    }

    private String showDirectory(File directory, int spaces) {
        StringBuilder stringBuilder = new StringBuilder();
        for (File file : directory.listFiles()) {
            stringBuilder.append(getSpaces(spaces)).append('\t').append('\n')
                    .append(getSpaces(spaces));
            stringBuilder.append(file.getName()).append(getSpaces(spaces));
            if (file.isDirectory()) {
                stringBuilder.append(showDirectory(file, spaces + 1));
            }
        }
        return stringBuilder.toString();
    }

    private String getSpaces(int spaces) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < spaces; i++) {
            stringBuilder.append('\t');
        }
        return stringBuilder.toString();
    }

    public String task05() {
        String path = resourceBundle.getString("task05");
        writeToFile(path, "hi i", " am illia");
        return readFromFile(path, 4, 9);
    }

    private void writeToFile(String path, String firstString, String secondString) {
        try (RandomAccessFile randomAccessFile =
                     new RandomAccessFile(path, "rw")) {
            FileChannel channel = randomAccessFile.getChannel();
            ByteBuffer[] byteBuffer = {ByteBuffer.wrap(firstString.getBytes()),
                    ByteBuffer.wrap(secondString.getBytes())};
            channel.write(byteBuffer);
            channel.close();
        } catch (IOException e) {
            view.showException(e);
        }
    }

    private String readFromFile(String path, int firstSize, int secondSize) {
        StringBuilder stringBuilder = new StringBuilder();
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(path, "rw")) {
            FileChannel channel = randomAccessFile.getChannel();
            ByteBuffer firstSentence = ByteBuffer.allocate(15);
            ByteBuffer secondSentence = ByteBuffer.allocate(21);
            ByteBuffer[] byteBuffer = {firstSentence, secondSentence};
            while (channel.read(byteBuffer) > 0) {
                byteBuffer[0].flip();
                while (byteBuffer[0].hasRemaining()) {
                    stringBuilder.append((char) byteBuffer[0].get());
                }
                byteBuffer[0].clear();

                byteBuffer[1].flip();
                while (byteBuffer[1].hasRemaining()) {
                    stringBuilder.append((char) byteBuffer[1].get());
                }
                byteBuffer[1].clear();
            }
            channel.close();
        } catch (IOException e) {
            view.showException(e);
        }
        return stringBuilder.toString();
    }

    public void task06() {
        Client client = new Client(view, "localhost", 4040);
        client.startClient();
    }
}
