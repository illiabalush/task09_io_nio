package com.balush.model.clientserver.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler extends Thread {
    private Socket socket;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;

    public ClientHandler(Socket socket) throws IOException {
        this.socket = socket;
        inputStream = new DataInputStream(socket.getInputStream());
        outputStream = new DataOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        while (true) {
            try {
                String word = inputStream.readUTF();
                Server.clientList.forEach(client -> send(client, word));
            } catch (IOException ex) {
                close();
                Server.clientList.remove(this);
                System.out.println("Client remove");
                break;
            }
        }
    }

    public void send(ClientHandler clientHandler, String msg) {
        try {
            clientHandler.outputStream.writeUTF(msg);
            clientHandler.outputStream.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void close() {
        try {
            socket.close();
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
