package com.balush.model.clientserver.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

class Server {
    private ServerSocket server;
    public static List<ClientHandler> clientList;

    public Server(int port) {
        try {
            server = new ServerSocket(port);
            clientList = new LinkedList<>();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void start() {
        System.out.println("Server started");
        while (true) {
            try {
                Socket socket = server.accept();
                System.out.println("Client accepted");
                ClientHandler clientHandler = new ClientHandler(socket);
                clientHandler.start();
                clientList.add(clientHandler);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
