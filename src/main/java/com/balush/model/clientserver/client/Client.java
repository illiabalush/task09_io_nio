package com.balush.model.clientserver.client;

import com.balush.view.View;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private View view;
    private Socket socket;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;

    private class ReadMsg extends Thread {
        @Override
        public void run() {
            String message;
            try {
                while (true) {
                    message = inputStream.readUTF();
                    view.showMessage(message);
                }
            } catch (IOException e) {
                close();
            }
        }
    }

    private class WriteMsg extends Thread {
        private Scanner clientStream = new Scanner(System.in);

        @Override
        public void run() {
            view.showMessage("Write message: ");
            while (true) {
                String clientMessage;
                try {
                    clientMessage = clientStream.nextLine();
                    if (clientMessage.equals("StopClient")) {
                        break;
                    }
                    outputStream.writeUTF(clientMessage);
                    outputStream.flush();
                } catch (IOException ex) {
                    close();
                    break;
                }
            }
            close();
        }
    }

    public Client(View view, String address, int port) {
        this.view = view;
        try {
            socket = new Socket(address, port);
            view.showMessage("Connected");
            inputStream = new DataInputStream(socket.getInputStream());
            outputStream = new DataOutputStream(socket.getOutputStream());
        } catch (IOException ex) {
            view.showMessage("Server is unavailable");
            close();
        }
    }

    public void startClient() {
        WriteMsg writeMsg = new WriteMsg();
        ReadMsg readMsg = new ReadMsg();
        writeMsg.start();
        readMsg.start();
        try {
            writeMsg.join();
            readMsg.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void close() {
        try {
            socket.close();
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
