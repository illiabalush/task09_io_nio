package com.balush.model.serializable;

import java.io.Serializable;
import java.util.Objects;

public class ExtendClass extends SerializableClass {
    private transient double d;
    private InnerClass innerClass;

    private class InnerClass implements Serializable {
        private String e;

        InnerClass(String e) {
            InnerClass.this.e = e;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            InnerClass that = (InnerClass) o;
            return Objects.equals(e, that.e);
        }

        @Override
        public int hashCode() {
            return Objects.hash(e);
        }
    }

    public ExtendClass(int a, String b, double c, double d, String e) {
        super(a, b, c);
        this.d = d;
        innerClass = new InnerClass(e);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ExtendClass that = (ExtendClass) o;
        return Double.compare(that.d, d) == 0 &&
                Objects.equals(innerClass, that.innerClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), d, innerClass);
    }

    @Override
    public String toString() {
        return super.toString() + " " + d + " " + innerClass.e;
    }
}
