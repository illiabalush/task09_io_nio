package com.balush.model.serializable;

import java.io.Serializable;
import java.util.Objects;

class SerializableClass implements Serializable {
    protected int a;
    protected String b;
    protected transient double c;

    public SerializableClass(int a, String b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SerializableClass that = (SerializableClass) o;
        return a == that.a &&
                Double.compare(that.c, c) == 0 &&
                Objects.equals(b, that.b);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b, c);
    }

    @Override
    public String toString() {
        return a + " " + b + " " + c;
    }
}
