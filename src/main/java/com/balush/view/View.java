package com.balush.view;

import com.balush.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class View {
    protected static Logger logger = LogManager.getLogger(View.class);
    protected Controller controller;
    public abstract void start();
    public abstract void showException(Exception ex);
    public abstract void showMessage(String msg);
}
