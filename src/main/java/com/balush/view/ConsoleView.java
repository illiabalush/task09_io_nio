package com.balush.view;

import com.balush.controller.Controller;
import com.balush.interfaces.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView extends View {
    private Scanner scanner;
    private Map<String, String> menu;
    private Map<String, Printable> menuItems;

    public ConsoleView() {
        controller = new Controller(this);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show serializable");
        menu.put("2", "2 - compare reading from file");
        menu.put("3", "3 - show comments after reading .java file");
        menu.put("4", "4 - show task directory");
        menu.put("5", "5 - write and read file using NIO");
        menu.put("6", "6 - start client");
        menu.put("Q", "Q - exit");
        menuItems = new LinkedHashMap<>();
        menuItems.put("1", this::showSerializable);
        menuItems.put("2", this::showReadingComparing);
        menuItems.put("3", this::showCommentsReadsFromFile);
        menuItems.put("4", this::showFileDirectory);
        menuItems.put("5", this::showUsingNIO);
        menuItems.put("6", this::showClientsWork);
    }

    private void showSerializable() {
        logger.info(controller.getTasks().task01());
    }

    private void showReadingComparing() {
        logger.info(controller.getTasks().task02());
    }

    private void showCommentsReadsFromFile() {
        logger.info(controller.getTasks().task03());
    }

    private void showFileDirectory() {
        logger.info(controller.getTasks().task04());
    }

    private void showUsingNIO() {
        logger.info(controller.getTasks().task05());
    }

    private void showClientsWork() {
        controller.getTasks().task06();
    }

    private void showMenu() {
        for (String s : menu.values()) {
            logger.info(s);
        }
    }

    @Override
    public void start() {
        String keyMenu;
        do {
            showMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuItems.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    @Override
    public void showMessage(String msg) {
        logger.info(msg);
    }

    @Override
    public void showException(Exception ex) {
        logger.info(ex.getMessage());
    }
}
