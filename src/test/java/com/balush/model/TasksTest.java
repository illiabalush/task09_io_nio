package com.balush.model;

import com.balush.model.serializable.ExtendClass;
import com.balush.view.ConsoleView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ResourceBundle;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class TasksTest {

    @Test
    public void testTask01() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String path = ResourceBundle.getBundle("path").getString("task01");
        Class clazz = Tasks.class;
        Tasks tasks = new Tasks(new ConsoleView());
        Class[] parametersWriteClass = {String.class, ExtendClass.class};
        Method methodWriteClass = clazz.getDeclaredMethod("writeClass", parametersWriteClass);
        methodWriteClass.setAccessible(true);
        ExtendClass extendClass = new ExtendClass(1, "S", 2.1, 3.2, "L");
        methodWriteClass.invoke(tasks, path, extendClass);
        Class[] parametersReadClass = {String.class};
        Method methodReadClass = clazz.getDeclaredMethod("readClass", parametersReadClass);
        methodReadClass.setAccessible(true);
        ExtendClass extendClassForComparing = new ExtendClass(1, "S", 0, 0, "L");
        assertEquals(methodReadClass.invoke(tasks, path), extendClassForComparing);
    }

    @Test
    public void testTask03() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class clazz = Tasks.class;
        Tasks tasks = new Tasks(new ConsoleView());
        Method testParseExtendedComment = clazz.getDeclaredMethod("parseExtendedComment", String.class, int.class);
        testParseExtendedComment.setAccessible(true);
        String codeSnippet = "rw/* hi /* eqw eqw */ test */";
        assertEquals("/* hi /* eqw eqw */", testParseExtendedComment.invoke(tasks, codeSnippet, 2));
        Method testParseSimpleComment = clazz.getDeclaredMethod("parseSimpleComment", String.class, int.class);
        testParseSimpleComment.setAccessible(true);
        codeSnippet = "rw/* hi// /* eqw eqw */ test */";
        assertEquals("// /* eqw eqw */ test */", testParseSimpleComment.invoke(tasks, codeSnippet, 7));
    }

    @Test
    public void testTask05() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String path = ResourceBundle.getBundle("path").getString("task05");
        Class clazz = Tasks.class;
        Tasks tasks = new Tasks(new ConsoleView());
        Class[] testParam = {String.class, String.class, String.class};
        Method testWriteToFile = clazz.getDeclaredMethod("writeToFile", testParam);
        testWriteToFile.setAccessible(true);
        String firstString = "now 12:36";
        String secondString = " but i alive";
        testWriteToFile.invoke(tasks, path, firstString, secondString);
        Class[] testParamRead = {String.class, int.class, int.class};
        Method testReadFromFile = clazz.getDeclaredMethod("readFromFile", testParamRead);
        testReadFromFile.setAccessible(true);
        assertEquals(firstString + secondString,
                testReadFromFile.invoke(tasks, path, firstString.length(), secondString.length()));
    }

}
